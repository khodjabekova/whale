from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _

from .managers import CustomUserManager


class CustomUser(AbstractUser):
    username = None
    email = models.EmailField(_("email address"), unique=True)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    # def save(self, *args, **kwargs):
    #     try:
    #         if kwargs['password']:
    #             self.set_password(kwargs['password'])
    #     except Exception:
    #         pass
    #     finally:
    #         super(CustomUser, self).save(*args, **kwargs)

    class Meta:
        db_table = 'account'
        verbose_name = _("User")
        verbose_name_plural = _("Users")
