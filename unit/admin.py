from django.contrib import admin
from unit.models import Folder, Unit, Translation, Word
# Register your models here.

class FolderAdmin(admin.ModelAdmin):
    list_display = ('name', )
    search_fields = ('name', )
    exclude = ('created_by', 'updated_by',)
    
admin.site.register(Folder, FolderAdmin)


class WordInline(admin.TabularInline):
    model = Word
    extra = 1
    exclude = ('created_by', 'updated_by',)


class TranslationInline(admin.StackedInline):
    model = Translation
    extra = 1
    exclude = ('created_by', 'updated_by',)


class UnitAdmin(admin.ModelAdmin):
    list_display = ('name', )
    search_fields = ('name', )
    exclude = ('created_by', 'updated_by',)
    inlines = (TranslationInline,)

admin.site.register(Unit, UnitAdmin)

class TranslationAdmin(admin.ModelAdmin):
    list_display = ('unit', )
    # search_fields = ('unit', )
    exclude = ('created_by', 'updated_by',)
    filter_horizontal = ('words', )
    # inlines = (WordInline,)

admin.site.register(Translation, TranslationAdmin)

class WordAdmin(admin.ModelAdmin):
    list_display = ('word', 'language')
    # search_fields = ('unit', )
    exclude = ('created_by', 'updated_by',)
    # inlines = (WordInline,)

admin.site.register(Word, WordAdmin)