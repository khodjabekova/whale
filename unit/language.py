
from django.utils.translation import gettext_lazy as _

LANGUAGE_CHOICE = (
    ('en', _('English')),
    ('ru',  _('Russian')),
    ('uz',  _('Uzbek')),
)