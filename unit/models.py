from django.db import models
from django.utils.translation import gettext_lazy as _
from unit.language import LANGUAGE_CHOICE
from baseapp.models import BaseModel


class Word(BaseModel):

    word = models.CharField(max_length=500, verbose_name=_('Word'))
    language = models.CharField(
        max_length=3, choices=LANGUAGE_CHOICE, default='en', verbose_name=_('Language'))

    def __str__(self):
        return self.word

    class Meta:
        db_table = 'word'
        verbose_name = _("Word")
        verbose_name_plural = _("Words")


class Unit(BaseModel):
    name = models.CharField(max_length=255, verbose_name=_('Unit name'))

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'unit'
        verbose_name = _("Unit")
        verbose_name_plural = _("Units")


class Translation(BaseModel):
    words = models.ManyToManyField(
        Word, related_name='translation', null=True, blank=True, verbose_name=_('Translation'))
    unit = models.ForeignKey(Unit, on_delete=models.CASCADE,
                             related_name='translations', verbose_name=_('Unit name'))
    index = models.IntegerField(default=0, verbose_name=_('Index number'))

    def __str__(self):
        return self.unit.name

    class Meta:
        ordering = ['-index']
        db_table = 'translation'
        verbose_name = _("Translation")
        verbose_name_plural = _("Translation")


class Folder(BaseModel):
    name = models.CharField(max_length=255, verbose_name=_('Folder Name'))
    units = models.ManyToManyField(Unit, related_name='folders')

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'folder'
        verbose_name = _("Folder")
        verbose_name_plural = _("Folders")
