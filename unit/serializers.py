from .models import Folder, Unit, Translation, Word
from django.db import IntegrityError, transaction

from rest_framework import serializers


class WordListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Word
        fields = ('word', 'language',)


class TranslationListSerializer(serializers.ModelSerializer):
    words = WordListSerializer(many=True)

    class Meta:
        model = Translation
        fields = ('id', 'words')


class UnitDetailsSerializer(serializers.ModelSerializer):
    translations = TranslationListSerializer(many=True)

    class Meta:
        model = Unit
        fields = ('id', 'name', 'translations')

    def create(self, validated_data):
            
        try:
            with transaction.atomic():
                translations = validated_data.pop('translations', None)
 
                unit = Unit.objects.create(**validated_data)
                if translations:
                    for tr in translations:
                        words = tr.pop('words', None)
                        
                        translation = Translation.objects.create(
                            unit=unit, **tr)
                        word_list = []
                        if words:
                            for wr in words:
                                word = Word.objects.create(**wr)
                                word_list.append(word)

                        translation.words.set(word_list)
                        translation.save()

                return unit

        except IntegrityError:
            pass

    def update(self, instance, validated_data):
           
        try:
            with transaction.atomic():
                translations = validated_data.pop('translations', None)
                
                for attr, value in validated_data.items():
                    setattr(instance, attr, value)
                if translations:
                    for tr in translations:
                        words = tr.pop('words', None)

                        translation = Translation.objects.create(
                            unit=instance, **tr)
                        word_list = []
                        if words:
                            for wr in words:
                                word = Word.objects.create(**wr)
                                word_list.append(word)

                        translation.words.set(word_list)
                        translation.save()

                instance.save()
                return instance

        except IntegrityError:
            pass


class UnitListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Unit
        fields = ('id', 'name',)


class FolderDetailsSerializer(serializers.ModelSerializer):
    units = UnitListSerializer(read_only=True, many=True)

    class Meta:
        model = Folder
        fields = ('id', 'name', 'units')


class FolderListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Folder
        fields = ('id', 'name',)
