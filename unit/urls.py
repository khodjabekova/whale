from django.urls import path

from . import views

urlpatterns = [
    path('folder', views.FolderListAPIView.as_view()),
    path('folder/<pk>', views.FolderDetailsAPIView.as_view()),
    path('unit', views.UnitListAPIView.as_view()),
    path('unit/<pk>', views.UnitDetailsAPIView.as_view()),
]
