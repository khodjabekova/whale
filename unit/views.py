from rest_framework.generics import ListAPIView, ListCreateAPIView, RetrieveAPIView ,RetrieveUpdateDestroyAPIView
from rest_framework.permissions import AllowAny

from unit import serializers
from unit.models import Folder, Unit, Translation, Word


class FolderListAPIView(ListAPIView):
    authentication_classes = []
    permission_classes = [AllowAny]
    serializer_class = serializers.FolderListSerializer
    queryset = Folder.objects.all()


class FolderDetailsAPIView(RetrieveAPIView):
    authentication_classes = []
    permission_classes = [AllowAny]
    serializer_class = serializers.FolderDetailsSerializer
    queryset = Folder.objects.prefetch_related('units').all()


class UnitListAPIView(ListCreateAPIView):
    authentication_classes = []
    permission_classes = [AllowAny]
    serializer_class = serializers.UnitDetailsSerializer
    queryset = Unit.objects.all()


class UnitDetailsAPIView(RetrieveUpdateDestroyAPIView):
    authentication_classes = []
    permission_classes = [AllowAny]
    serializer_class = serializers.UnitDetailsSerializer
    queryset = Unit.objects.all()
